﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMover : MonoBehaviour {

	public bool rotate = true;
	public Vector3 rotationValue;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if( rotate ){
			transform.Rotate( rotationValue * Time.deltaTime );
		}
	}
}
