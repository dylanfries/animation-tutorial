﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Needed to use UI
using UnityEngine.UI;

public class AnimationBlendController : MonoBehaviour {

	public Animator anim;
	public float blendValue;

	public Slider sliderReference;
	public Text textDisplay;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		SliderUpdated();
	}

	public void SliderUpdated(){
		blendValue = sliderReference.value;
		anim.SetFloat("Blend", blendValue);
		textDisplay.text = blendValue.ToString("F2"); // write the number to the Text element. The F2 is a float decimal place 
	}
}
