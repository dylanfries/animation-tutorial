﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnPlay : MonoBehaviour {

	public GameObject go;

	// Use this for initialization
	void Start () {
		go = gameObject;
		go.SetActive(false);
	}

}
