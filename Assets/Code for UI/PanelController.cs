﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour {

	public List<GameObject> panels;
		
	void Awake(){
		//panels = new List<GameObject>();

	}

	void Start(){
		OpenPanel(0);
	}

	// Use this for initialization
	public void CloseAllPanels () {
		foreach(GameObject panel in panels){
			panel.SetActive(false);
		}
	}
	
	// Update is called once per frame
	public void OpenPanel (int panelID) {
		if( panels != null){
			if (panelID >= 0 && panelID < panels.Count && panels[panelID] != null ) {

				CloseAllPanels();
				panels[panelID].SetActive(true);

			}else{
				Debug.LogWarning("Panel ID is out of range: " + panelID + " count: " + panels.Count);
			}
		}else{
			Debug.LogWarning("Panels List is null");
		}

	}
}
